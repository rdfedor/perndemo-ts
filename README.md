# Perndemo-ts

## Contents

* [Backend](./backend/README.md) - Services side of the boilerplate project w/ JWT authentication and role based access control

## Setup

### Updating the Machine Host File

Add the following mappings to your host file for the docker-compose,

```
127.0.0.1   traefik.perndemo-ts.lan backend.perndemo-ts.lan adminer.perndemo-ts.lan 
```

### Start the Services

Use docker-compose to build and start the various containers,

```sh
docker-compose up -d
```

### Managing the Database with Adminer

Adminer comes baked with the project in order to handle managing the data within containers.  Visit `adminer.perndemo-ts.lan` to open up the page and login using the credentials defined in the compose file.
