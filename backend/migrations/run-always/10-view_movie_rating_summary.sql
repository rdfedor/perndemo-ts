CREATE OR REPLACE VIEW movie_rating_summary AS
SELECT
  m.movie_uuid,
  avg(r.rating) as avg_rating
FROM movie m
LEFT JOIN ratings r ON (m.movie_uuid = r.movie_uuid)
GROUP BY m.movie_uuid
