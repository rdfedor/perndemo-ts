import { MigrationInterface, QueryRunner } from 'typeorm'

export class movieDbSetup1621304683099 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE "movie" (
          movie_uuid uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
          title TEXT NOT NULL,
          rating_classification_id SMALLINT NOT NULL DEFAULT 1,
          description TEXT NULL,
          thumbnail_url TEXT NULL,
          poster_url TEXT NULL,
          release_date DATE NOT NULL,
          created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
        );
      `)

    await queryRunner.query(`
        CREATE TABLE "rating_classification" (
          rating_classification_id SMALLSERIAL NOT NULL PRIMARY KEY,
          short_name VARCHAR(5) NOT NULL,
          long_name TEXT NOT NULL,
          UNIQUE(short_name)
        );
      `)

    await queryRunner.query(`
        INSERT INTO "rating_classification" (short_name, long_name) VALUES 
          ('UR/NR', 'Unrated / Not Rated'),
          ('G', 'General Audience'),
          ('PG', 'Parent Guidance Suggested'),
          ('PG-13', 'Parents Strongly Cautioned'),
          ('R', 'Restricted'),
          ('NC-17', 'No one 17 and under admitted')
      `)

    await queryRunner.query(`
        CREATE TABLE "crew_role" (
          crew_role_id SMALLSERIAL NOT NULL PRIMARY KEY,
          title TEXT NOT NULL UNIQUE,
          credit_order SMALLINT
        );
      `)

    await queryRunner.query(`
        INSERT INTO "crew_role" (title, credit_order) VALUES 
          ('Producer', 10),
          ('Writer', 30),
          ('Director', 20),
          ('Actor', 100)
      `)

    await queryRunner.query(`
        CREATE TABLE "person" (
          person_uuid uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
          name TEXT NOT NULL,
          location GEOGRAPHY NULL,
          born DATE NULL,
          died DATE NULL,
          description TEXT
        );
      `)

    await queryRunner.query(`
        CREATE TABLE "crew" (
          movie_uuid UUID NOT NULL REFERENCES movie(movie_uuid),
          person_uuid UUID NOT NULL REFERENCES person(person_uuid),
          crew_role_id SMALLINT NOT NULL REFERENCES crew_role(crew_role_id),
          name TEXT NULL,
          notes TEXT,
          UNIQUE(movie_uuid, person_uuid, crew_role_id)
        );
      `)

    await queryRunner.query(`
        CREATE TABLE "ratings" (
          movie_uuid UUID NOT NULL REFERENCES movie(movie_uuid),
          user_uuid UUID NOT NULL REFERENCES "user"(user_uuid),
          rating SMALLINT NULL,
          created_at TIMESTAMP not NULL DEFAULT CURRENT_TIMESTAMP,
          UNIQUE(movie_uuid, user_uuid)
        );
      `)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DROP TABLE ratings')
    await queryRunner.query('DROP TABLE crew')
    await queryRunner.query('DROP TABLE person')
    await queryRunner.query('DROP TABLE crew_role')
    await queryRunner.query('DROP TABLE rating_classification')
    await queryRunner.query('DROP TABLE movie')
  }
}
