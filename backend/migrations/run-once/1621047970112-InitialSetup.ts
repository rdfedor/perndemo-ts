import { MigrationInterface, QueryRunner } from 'typeorm'

export class Createuser1621047970112 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
    `)
    await queryRunner.query(
      'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO perndemouser;',
    )
    await queryRunner.query(`
      CREATE TABLE "user" (
        user_uuid uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
        email VARCHAR(254) NOT NULL,
        password CHAR(60) NOT NULL,
        first_name VARCHAR(64) NULL,
        last_name VARCHAR(64) NULL,
        verified_email BOOLEAN NOT NULL DEFAULT FALSE,
        created_at TIMESTAMP NOT NULL DEFAULT NOW(),
        UNIQUE(email)
      );
    `)
    await queryRunner.query(`
      CREATE TABLE "role" (
        role_id SMALLSERIAL PRIMARY KEY,
        name VARCHAR(25) NOT NULL,
        UNIQUE(name)
      )
    `)
    await queryRunner.query(`
      INSERT INTO "role" (name) VALUES 
        ('BANNED'),
        ('ADMIN')
    `)
    await queryRunner.query(`
      CREATE TABLE "user_role" (
        user_uuid uuid NOT NULL,
        role_id SMALLINT NOT NULL,
        target_uuid uuid NULL,
        target_id BIGINT NULL,
        created_at TIMESTAMP NOT NULL DEFAULT NOW(),
        UNIQUE(user_uuid, role_id, target_uuid, target_id),
        CONSTRAINT fk_user_roles_user_uuid
          FOREIGN KEY(user_uuid) 
            REFERENCES "user"(user_uuid)
            ON DELETE CASCADE,
        CONSTRAINT fk_user_roles_role_id
          FOREIGN KEY(role_id) 
            REFERENCES "role"(role_id)
            ON DELETE CASCADE
      )
    `)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE user_role;`)
    await queryRunner.query(`DROP TABLE role;`)
    await queryRunner.query(`DROP TABLE user;`)
  }
}
