import * as express from 'express'
import * as expressRequestId from 'express-request-id'
import ExpressPlugin from 'src/interface/expressPlugin'

export class ExpressLoaderConfiguration {
  plugins: ExpressPlugin[]
}

export default class ExpressLoader {
  private app: express.Express
  private configuration: ExpressLoaderConfiguration

  constructor(configuration: ExpressLoaderConfiguration) {
    const app = express()

    app.use(expressRequestId())
    app.use(express.json())

    this.app = app
    this.configuration = configuration
  }

  loadPlugins(): void {
    this.configuration.plugins.forEach((plugin: ExpressPlugin) => {
      console.log(`Loading ${plugin.constructor.name}`)
      plugin.use(this.app)
    })
  }

  getApp(): express.Express {
    return this.app
  }
}
