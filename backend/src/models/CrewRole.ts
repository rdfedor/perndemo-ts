import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  JoinColumn,
} from 'typeorm'
import { Crew } from './Crew'

@Entity('crew_role')
export class CrewRole {
  @PrimaryGeneratedColumn({ name: 'crew_role_id' })
  id: string

  @Column()
  title: string

  @Column({ name: 'credit_order' })
  creditOrder: number

  @OneToMany(() => Crew, crew => crew)
  @JoinColumn({ name: 'crew_role_id' })
  crew: Crew[]
}
