import { Exclude } from 'class-transformer'
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
  OneToOne,
} from 'typeorm'
import { Crew } from './Crew'
import { MovieRatingSummary } from './MovieRatingSummary'
import { RatingClassification } from './RatingClassification'

@Entity('movie')
export class Movie {
  @PrimaryGeneratedColumn({ name: 'movie_uuid' })
  uuid: string

  @Column()
  title: string

  @Column()
  description: string

  @Exclude()
  @Column({ name: 'rating_classification_id' })
  ratingClassificationId: number

  @ManyToOne(
    () => RatingClassification,
    ratingClassification => ratingClassification.id,
  )
  @JoinColumn({ name: 'rating_classification_id' })
  ratingClassification: RatingClassification

  @OneToMany(() => Crew, crew => crew.movie)
  @JoinColumn({ name: 'movie_uuid' })
  crew: Crew[]

  @OneToOne(() => MovieRatingSummary)
  @JoinColumn({ name: 'movie_uuid' })
  ratingSummary: MovieRatingSummary

  @Column({ name: 'thumbnail_url' })
  thumbnailUrl: string

  @Column({ name: 'poster_url' })
  posterUrl: string

  @Column({ name: 'release_date', type: 'date' })
  releaseDate: Date

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
  createdAt: Date

  @CreateDateColumn({ name: 'updated_at', type: 'timestamp' })
  updatedAt: Date
}
