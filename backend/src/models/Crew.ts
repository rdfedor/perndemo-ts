import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm'
import { CrewRole } from './CrewRole'
import { Movie } from './Movie'
import { Person } from './Person'

@Entity('crew')
export class Crew {
  @PrimaryColumn({ name: 'movie_uuid' })
  movieUuid: string

  @PrimaryColumn({ name: 'person_uuid' })
  personUuid: string

  @ManyToOne(() => Movie, movie => movie.crew)
  @JoinColumn({ name: 'movie_uuid' })
  movie: Movie

  @ManyToOne(() => Person, person => person.crew)
  @JoinColumn({ name: 'person_uuid' })
  person: Person

  @Column({ name: 'crew_role_id' })
  crewRoleId: number

  @ManyToOne(() => CrewRole, crewRole => crewRole.crew)
  @JoinColumn({ name: 'crew_role_id' })
  role: CrewRole

  @Column()
  name: string

  @Column()
  notes: string
}
