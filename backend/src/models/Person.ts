import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Point } from 'geojson'
import { Crew } from './Crew'

@Entity('person')
export class Person {
  @PrimaryGeneratedColumn({ name: 'person_uuid' })
  uuid: string

  @OneToMany(() => Crew, crew => crew.person)
  @JoinColumn({ name: 'person_uuid' })
  crew: Crew[]

  @Column()
  name: string

  @Column({
    type: 'geography',
    spatialFeatureType: 'Point',
    srid: 4326,
    nullable: true,
  })
  location: Point

  @Column({ name: 'born', type: 'date' })
  born: Date

  @Column({ name: 'died', type: 'date' })
  died: Date

  @Column()
  description: string
}
