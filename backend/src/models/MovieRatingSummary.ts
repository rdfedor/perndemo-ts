import { Exclude } from 'class-transformer'
import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm'
import { Movie } from './Movie'

@Entity('movie_rating_summary')
export class MovieRatingSummary {
  @Exclude()
  @PrimaryColumn({ name: 'movie_uuid' })
  movieUuid: string

  @OneToOne(() => Movie)
  @JoinColumn({ name: 'movie_uuid' })
  movie: Movie

  @Column({ name: 'avg_rating' })
  avgRating: number
}
