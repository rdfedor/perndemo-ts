import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { UserRole } from './UserRole'

@Entity('role')
export class Role {
  @PrimaryGeneratedColumn({ name: 'role_id' })
  id: number

  @Column()
  name: string

  @OneToMany(() => UserRole, userRole => userRole.roleId)
  @JoinColumn({ name: 'role_id' })
  userRoles: UserRole
}
