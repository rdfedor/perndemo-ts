import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm'
import { Movie } from './Movie'
import { Person } from './Person'
import { User } from './User'

@Entity('rating')
export class Rating {
  @PrimaryColumn({ name: 'movie_uuid' })
  movieUuid: string

  @PrimaryColumn({ name: 'user_uuid' })
  userUuid: string

  @ManyToOne(() => Movie, movie => movie.crew)
  @JoinColumn({ name: 'movie_uuid' })
  movie: Movie

  @ManyToOne(() => User, user => user.ratings)
  @JoinColumn({ name: 'user_uuid' })
  user: Person

  @Column()
  rating: number

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
  createdAt: Date
}
