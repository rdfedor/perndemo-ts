import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  JoinColumn,
} from 'typeorm'
import { Movie } from './Movie'

@Entity('rating_classification')
export class RatingClassification {
  @PrimaryGeneratedColumn({ name: 'rating_classification_id' })
  id: string

  @Column({ name: 'short_name' })
  shortName: string

  @Column({ name: 'long_name' })
  longName: string

  @OneToMany(() => Movie, movie => movie.ratingClassificationId)
  @JoinColumn({ name: 'rating_classification_id' })
  movies: Movie[]
}
