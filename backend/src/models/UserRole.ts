import {
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm'
import { User } from './User'
import { Role } from './Role'
import { Exclude } from 'class-transformer'

@Entity('user_role')
export class UserRole {
  @Exclude()
  @PrimaryColumn({ name: 'user_uuid' })
  userUuid: string

  @ManyToOne(() => User, user => user.roles)
  @JoinColumn({ name: 'user_uuid' })
  user: User

  @Exclude()
  @PrimaryColumn({ name: 'role_id' })
  roleId: number

  @ManyToOne(() => Role, role => role.userRoles, {
    eager: true,
  })
  @JoinColumn({ name: 'role_id' })
  role: Role

  @PrimaryColumn({ name: 'target_uuid' })
  targetUuid: string

  @PrimaryColumn({ name: 'target_id' })
  targetId: number

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
  createdAt: Date
}
