import { Exclude } from 'class-transformer'
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Rating } from './Rating'
import { UserRole } from './UserRole'

@Entity('user')
export class User {
  @PrimaryGeneratedColumn({ name: 'user_uuid' })
  uuid: string

  @Column()
  email: string

  @Exclude()
  @Column()
  password: string

  @Column({ name: 'first_name' })
  firstName: string

  @Column({ name: 'last_name' })
  lastName: string

  @Column({ name: 'verified_email' })
  verifiedEmail: boolean

  @OneToMany(() => Rating, rating => rating.user)
  @JoinColumn({ name: 'user_uuid' })
  ratings: Rating[]

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
  createdAt: Date

  @OneToMany(() => UserRole, userRole => userRole.user, {
    eager: true,
  })
  @JoinColumn({ name: 'user_uuid' })
  roles: UserRole[]
}
