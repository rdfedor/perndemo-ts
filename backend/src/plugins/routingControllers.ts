import { Express } from 'express'
import ExpressPlugin from 'src/interface/expressPlugin'

import { useExpressServer, useContainer, Action } from 'routing-controllers'
import { JwtService } from 'src/services/jwt'
import { Container } from 'typedi'
import { JWT_ACCESS_TOKEN_DURATION } from 'src/config'
import TokenPayload from 'src/validators/token-payload'
import { CONTROLLERS_DIR, MIDDLEWARE_DIR } from 'src/constant'
import { AccessDeniedError } from 'src/error'
import UserService from 'src/services/user'

export class RoutingControllersPlugin implements ExpressPlugin {
  private jwtService: JwtService
  private userService: UserService

  constructor() {
    this.jwtService = Container.get(JwtService)
    this.userService = Container.get(UserService)
  }

  public use(app: Express): void {
    useContainer(Container)

    useExpressServer(app, {
      authorizationChecker: (action: Action, roles: string[]) =>
        this.authorizationChecker(action, roles),
      currentUserChecker: (action: Action) => this.currentUserChecker(action),
      classTransformer: true,
      controllers: [CONTROLLERS_DIR],
      middlewares: [MIDDLEWARE_DIR],
      cors: true,
      defaultErrorHandler: false,
    })
  }

  private async authorizationChecker(action: Action, roles: string[]) {
    if (action.request.headers['authorization']) {
      const [transport, token] =
        action.request.headers['authorization'].split(' ')

      if (transport.toLowerCase() === 'bearer') {
        let tokenPayload
        try {
          tokenPayload = <TokenPayload>this.jwtService.verify(token, {
            maxAge: JWT_ACCESS_TOKEN_DURATION,
          })
        } catch (err) {
          throw new AccessDeniedError(err.message)
        }

        if (roles.length) {
          if (
            !tokenPayload.roles.filter(role => roles.indexOf(role) !== -1)
              .length
          ) {
            return false
          }
        }

        action.request.token = tokenPayload
        return true
      }
    }

    return false
  }

  private async currentUserChecker(action: Action) {
    return await this.userService.getUser(action.request.token)
  }
}
