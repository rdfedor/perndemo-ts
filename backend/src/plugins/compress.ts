import { Express } from 'express'
import ExpressPlugin from 'src/interface/expressPlugin'
import * as compression from 'compression'
import { ENV } from 'src/config'
import { ENVIRONMENT_PROD } from 'src/constant'

export class CompressPlugin implements ExpressPlugin {
  public use(app: Express): void {
    if (ENV === ENVIRONMENT_PROD) {
      app.use(compression())
    }
  }
}
