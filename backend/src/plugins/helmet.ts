import { Express } from 'express'
import ExpressPlugin from 'src/interface/expressPlugin'
import * as helmet from 'helmet'

export class HelmetPlugin implements ExpressPlugin {
  public use(app: Express): void {
    app.use(helmet.dnsPrefetchControl())
    app.use(helmet.expectCt())
    app.use(helmet.frameguard())
    app.use(helmet.hidePoweredBy())
    app.use(helmet.hsts())
    app.use(helmet.ieNoOpen())
    app.use(helmet.noSniff())
    app.use(helmet.permittedCrossDomainPolicies())
    app.use(helmet.referrerPolicy())
    app.use(helmet.xssFilter())
  }
}
