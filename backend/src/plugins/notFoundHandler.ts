import { Express } from 'express'
import ExpressPlugin from 'src/interface/expressPlugin'

export class NotFoundHandlerPlugin implements ExpressPlugin {
  public use(app: Express): void {
    app.use((_req, res) => {
      if (!res.statusMessage) {
        res.status(404).json({
          message: 'Page not found',
        })
      }
    })
  }
}
