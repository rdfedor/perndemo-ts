import { Express } from 'express'
import ExpressPlugin from 'src/interface/expressPlugin'

import { getMetadataArgsStorage } from 'routing-controllers'
import { validationMetadatasToSchemas } from 'class-validator-jsonschema'
import { defaultMetadataStorage } from 'class-transformer/cjs/storage'
import * as swaggerUi from 'swagger-ui-express'
import { routingControllersToSpec } from 'routing-controllers-openapi'
import { importClassesFromDirectories } from '../utils/importClassesFromDirectories'
import { CONTROLLERS_DIR } from 'src/constant'
import * as packageJson from 'package.json'

export class SwaggerPlugin implements ExpressPlugin {
  public use(app: Express): void {
    const schemas = validationMetadatasToSchemas({
      classTransformerMetadataStorage: defaultMetadataStorage,
      refPointerPrefix: '#/components/schemas/',
    })

    const routerControllerOptions = {
      controllers: [CONTROLLERS_DIR, `src/validators/**/*.ts`],
    }

    const spec = routingControllersToSpec(
      getMetadataArgsStorage(),
      {
        controllers: importClassesFromDirectories(
          routerControllerOptions.controllers,
        ),
      },
      {
        openapi: '3.0.0',
        info: {
          description: 'Documentation for the Perndemo-ts backend api.',
          title: 'API Documentation',
          version: packageJson.version,
        },
        components: {
          schemas,
          securitySchemes: {
            bearerAuth: {
              type: 'http',
              scheme: 'bearer',
            },
          },
        },
        servers: [
          {
            url: 'http://backend.perndemo-ts.lan/',
            description: 'Development server',
          },
        ],
      },
    )

    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(spec))
  }
}
