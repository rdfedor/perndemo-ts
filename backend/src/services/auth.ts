import { Service } from 'typedi'
import { NotFoundError, AccessDeniedError } from 'src/error'
import {
  CreateUserRequestModel,
  GetUserByEmailRequestModel,
} from 'src/validators/user'
import {
  AuthenticateUserRequest,
  RefreshTokenRequest,
} from 'src/validators/auth'
import {
  SendPasswordResetRequest,
  ResetPasswordRequest,
} from 'src/validators/password-reset'
import { SendEmailVerificationRequestToken } from 'src/validators/email-verification'
import { ROLE_BANNED } from 'src/constant'
import { JwtService } from './jwt'
import { CryptService } from './crypt'
import { plainToClass } from 'class-transformer'
import { BadRequestError } from 'routing-controllers'
import { LoginToken } from 'src/validators/login-token'
import { RequestResult } from 'src/interface/request-result'
import UserService from './user'
import { JwtToken } from 'src/interface/jwt-request'
import { User } from 'src/models/User'
import { UserRepository } from 'src/repositories/UserRepository'
import { InjectRepository } from 'typeorm-typedi-extensions'
import { JsonWebTokenError } from 'jsonwebtoken'
import {
  JWT_PASSWORD_RESET_DURATION,
  JWT_REFRESH_TOKEN_DURATION,
  JWT_VERIFY_EMAIL_DURATION,
} from 'src/config'

@Service()
export default class AuthService {
  constructor(
    @InjectRepository(User) private userRepository: UserRepository,
    private userService: UserService,
    private jwtService: JwtService,
    private cryptService: CryptService,
  ) {}

  public async processRegisterUser(
    req: CreateUserRequestModel,
  ): Promise<LoginToken> {
    const jwtRequest = plainToClass(
      SendEmailVerificationRequestToken,
      this.jwtService.verify(req.token, {
        maxAge: JWT_VERIFY_EMAIL_DURATION,
        subject: 'verify-email',
      }),
    )

    const challengeCodeMatches = await this.cryptService.compare(
      req.challengeCode,
      jwtRequest.challengeCode,
    )

    if (!challengeCodeMatches) {
      throw new BadRequestError('Challenge code does not match')
    }

    const encryptedPassword = await this.cryptService.hashText(req.password)

    const { firstName, lastName } = req
    const { email } = jwtRequest

    let user: User = null
    try {
      user = this.userRepository.create({
        firstName,
        lastName,
        password: encryptedPassword,
        email,
        verifiedEmail: true,
      })
      await this.userRepository.save(user)
    } catch (ex) {
      throw new BadRequestError('User already exists by that email')
    }

    return this.getLoginToken(user)
  }

  public async processSendPasswordResetRequest(
    req: SendPasswordResetRequest,
  ): Promise<string> {
    return this.jwtService.sign(
      {
        email: req.email,
      },
      {
        subject: 'password-reset',
      },
    )
  }

  public async processResetPasswordRequest(
    req: ResetPasswordRequest,
  ): Promise<RequestResult> {
    const token = plainToClass(
      SendPasswordResetRequest,
      this.jwtService.verify(req.token, {
        subject: 'password-reset',
        maxAge: JWT_PASSWORD_RESET_DURATION,
      }),
    )

    const encryptedPassword = await this.cryptService.hashText(req.password)

    await this.userRepository.update(
      {
        email: token.email,
      },
      {
        password: encryptedPassword,
      },
    )

    return {
      status: 1,
    }
  }

  public async processAuthenticateUserRequest(
    req: AuthenticateUserRequest,
  ): Promise<LoginToken> {
    const user = await this.userService.getUserByEmail(
      plainToClass(GetUserByEmailRequestModel, {
        email: req.email,
      }),
    )

    if (!user.email) {
      throw new NotFoundError('User not found')
    }

    const passwordMatches = await this.cryptService.compare(
      req.password,
      user.password,
    )

    if (!passwordMatches) {
      throw new AccessDeniedError('Invalid password')
    }

    return this.getLoginToken(user)
  }

  public async processRefreshToken(
    req: RefreshTokenRequest,
  ): Promise<LoginToken> {
    let token = null
    try {
      token = <JwtToken>this.jwtService.verify(req.token, {
        maxAge: JWT_REFRESH_TOKEN_DURATION,
        subject: 'refresh-token',
      })
    } catch (err) {
      if (err instanceof JsonWebTokenError) {
        throw new AccessDeniedError('Access denied. Invalid refresh token.')
      }
    }
    const user = await this.userService.getUser(token.userUuid)

    return this.getLoginToken(user)
  }

  private getLoginToken(user: User): LoginToken {
    const userRoles = (user.roles || []).map(role => role.role.name)
    if (userRoles.indexOf(ROLE_BANNED) > -1) {
      throw new AccessDeniedError('User has been banned.')
    }

    return {
      accessToken: this.jwtService.sign(
        {
          userUuid: user.uuid,
          email: user.email,
          roles: userRoles,
        },
        {
          subject: 'access-token',
        },
      ),
      refreshToken: this.jwtService.sign(
        {
          userUuid: user.uuid,
        },
        {
          subject: 'refresh-token',
        },
      ),
      iat: Math.floor(+new Date() / 1000),
      roles: userRoles,
      userUuid: user.uuid,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      verifiedEmail: user.verifiedEmail,
    }
  }
}
