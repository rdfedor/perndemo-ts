import * as Debug from 'debug'
import { PersonRepository } from '../repositories/PersonRepository'
import { Person } from 'src/models/Person'
import { InjectRepository } from 'typeorm-typedi-extensions'
import { Service } from 'typedi'
import { PersonRequest } from 'src/validators/person'
import { validateOrReject } from 'class-validator'
import { NotFoundError } from 'src/error'

const debug = Debug('backend:service:person')

@Service()
export default class PersonService {
  constructor(
    @InjectRepository(Person) private personRepository: PersonRepository,
  ) {}

  public async getPersons(): Promise<Person[]> {
    return await this.personRepository.find()
  }

  public async getPerson(personUuid: string): Promise<Person> {
    return await this.personRepository
      .createQueryBuilder('person')
      .leftJoinAndSelect('person.crew', 'crew')
      .leftJoinAndSelect('crew.movie', 'movie')
      .leftJoinAndSelect('crew.role', 'crewRole')
      .where('person.person_uuid = :personUuid', { personUuid })
      .getOne()
  }

  public async deletePerson(personUuid: string): Promise<void> {
    const result = await this.personRepository.delete(personUuid)

    if (!result.affected) {
      throw new NotFoundError('Movie not found')
    }
  }

  public async addPerson(req: PersonRequest): Promise<Person> {
    debug(`Adding person`, JSON.stringify(req))
    await validateOrReject(req)

    const person = this.personRepository.create({
      name: req.name,
      description: req.description,
      born: req.born,
      died: req.died,
    })

    return await this.personRepository.save(person)
  }

  public async updatePerson(
    personUuid: string,
    req: PersonRequest,
  ): Promise<Person> {
    const person = await this.personRepository.findOne(personUuid)

    if (!person) {
      throw new NotFoundError('Movie not found')
    }

    person.name = req.name
    person.description = req.description
    person.location = req.location
      ? {
          type: 'Point',
          coordinates: req.location.split(',').map(val => parseFloat(val)),
        }
      : null
    person.born = req.born ? new Date(req.born) : null
    person.died = req.died ? new Date(req.died) : null

    return await this.personRepository.save(person)
  }
}
