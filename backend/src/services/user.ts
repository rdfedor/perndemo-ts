import { plainToClass } from 'class-transformer'
import * as Debug from 'debug'
import { Service } from 'typedi'
import { validateOrReject } from 'class-validator'
import { ENV, JWT_VERIFY_EMAIL_DURATION } from '../config'
import { ENVIRONMENT_DEV, ALL_ROLES } from '../constant'
import { JwtService } from './jwt'
import { MathService } from './math'
import { CryptService } from './crypt'
import { BadRequestError, NotFoundError } from '../error'
import { RequestResult } from '../interface/request-result'
import { UserRoleRequest, GetUserByEmailRequestModel } from '../validators/user'
import {
  SendEmailVerificationRequest,
  SendEmailVerificationRequestToken,
  EmailVerificationRequest,
} from '../validators/email-verification'
import { UserRepository } from '../repositories/UserRepository'
import { User } from 'src/models/User'
import { InjectRepository } from 'typeorm-typedi-extensions'

const debug = Debug('backend:service:user')

@Service()
export default class UserService {
  constructor(
    @InjectRepository(User) private userRepository: UserRepository,
    private jwtService: JwtService,
    private cryptService: CryptService,
    private mathService: MathService,
  ) {}

  public async getUsers(): Promise<User[]> {
    return this.userRepository.find()
  }

  public async getUser(userUuid: string): Promise<User> {
    const user = await this.userRepository.findOne(userUuid)

    if (!user) {
      throw new NotFoundError('User not found')
    }

    return user
  }

  public async getUserByEmail(req: GetUserByEmailRequestModel): Promise<User> {
    await validateOrReject(req)

    const user = await this.userRepository.findOne({
      email: req.email,
    })

    if (!user) {
      throw new NotFoundError('User not found')
    }

    return user
  }

  public async deleteUser(userUuid: string): Promise<RequestResult> {
    const result = await this.userRepository.delete(userUuid)

    if (!result.affected) {
      throw new NotFoundError('User not found')
    }

    return {
      status: 1,
    }
  }

  public async processSendEmailVerificationRequest(
    req: SendEmailVerificationRequest,
  ): Promise<string> {
    const challengeCode = new Array(6)
      .fill(1, 0, 6)
      .map(() => this.mathService.randomIntenger(0, 9).toString())
      .join('')

    if (ENV === ENVIRONMENT_DEV) {
      debug(`Challenge Code: ${challengeCode}`)
    }

    return this.jwtService.sign(
      {
        challengeCode: await this.cryptService.hashText(challengeCode),
        email: req.email,
      },
      {
        subject: 'verify-email',
      },
    )
  }

  public async processEmailVerificationRequest(
    req: EmailVerificationRequest,
  ): Promise<RequestResult> {
    const jwtRequest = plainToClass(
      SendEmailVerificationRequestToken,
      this.jwtService.verify(req.token, {
        subject: 'verify-email',
        maxAge: JWT_VERIFY_EMAIL_DURATION,
      }),
    )

    const challengeCodeMatches = await this.cryptService.compare(
      req.challengeCode,
      jwtRequest.challengeCode,
    )

    if (!challengeCodeMatches) {
      throw new BadRequestError('Challenge code does not match')
    }

    const result = await this.userRepository.update(jwtRequest.userUuid, {
      email: jwtRequest.email,
      verifiedEmail: true,
    })

    if (!result.affected) {
      throw new BadRequestError('User email already verified')
    }

    return {
      status: 1,
    }
  }

  public async addRole(req: UserRoleRequest): Promise<RequestResult> {
    await validateOrReject(req)

    if (ALL_ROLES.indexOf(req.roleName) === -1) {
      throw new BadRequestError('Invalid role')
    }

    await this.userRepository.addRole(req.userUuid, req.roleName)

    return {
      status: 1,
    }
  }

  public async deleteRole(req: UserRoleRequest): Promise<RequestResult> {
    await validateOrReject(req)

    if (ALL_ROLES.indexOf(req.roleName) === -1) {
      throw new BadRequestError('Invalid role')
    }

    await this.userRepository.deleteRole(req.userUuid, req.roleName)

    return {
      status: 1,
    }
  }
}
