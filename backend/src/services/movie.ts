import * as Debug from 'debug'
import { MovieRepository } from '../repositories/MovieRepository'
import { Movie } from 'src/models/Movie'
import { InjectRepository } from 'typeorm-typedi-extensions'
import { Service } from 'typedi'
import {
  AddCrewRequest,
  AddRatingRequest,
  MovieRequest,
  RemoveCrewRequest,
} from 'src/validators/movie'
import { validateOrReject } from 'class-validator'
import { NotFoundError } from 'src/error'
import { Crew } from 'src/models/Crew'
import { CrewRepository } from 'src/repositories/CrewRepository'
import { Rating } from 'src/models/Rating'
import { RatingRepository } from 'src/repositories/RatingRepository'

const debug = Debug('backend:service:movie')

@Service()
export default class MovieService {
  constructor(
    @InjectRepository(Movie) private movieRepository: MovieRepository,
    @InjectRepository(Crew) private crewRepository: CrewRepository,
    @InjectRepository(Rating) private ratingRepository: RatingRepository,
  ) {}

  public async getMovies(): Promise<Movie[]> {
    return await this.movieRepository
      .createQueryBuilder('movie')
      .innerJoinAndSelect('movie.ratingClassification', 'ratingClassification')
      .innerJoinAndSelect('movie.ratingSummary', 'movieRatingSummary')
      .getMany()
  }

  public async getMovie(movieUuid: string): Promise<Movie> {
    return await this.movieRepository
      .createQueryBuilder('movie')
      .innerJoinAndSelect('movie.ratingClassification', 'ratingClassification')
      .innerJoinAndSelect('movie.ratingSummary', 'movieRatingSummary')
      .leftJoinAndSelect('movie.crew', 'crew')
      .leftJoinAndSelect('crew.person', 'person')
      .leftJoinAndSelect('crew.role', 'crewRole')
      .where('movie.movie_uuid = :movieUuid', { movieUuid })
      .getOne()
  }

  public async deleteMovie(movieUuid: string): Promise<void> {
    const result = await this.movieRepository.delete(movieUuid)

    if (!result.affected) {
      throw new NotFoundError('Movie not found')
    }
  }

  public async addMovie(req: MovieRequest): Promise<Movie> {
    debug(`Adding movie`, JSON.stringify(req))
    await validateOrReject(req)

    const movie = this.movieRepository.create({
      title: req.title,
      description: req.description,
      ratingClassificationId: req.ratingClassificationId,
      thumbnailUrl: req.thumbnailUrl,
      posterUrl: req.posterUrl,
      releaseDate: req.releaseDate,
    })

    return await this.movieRepository.save(movie)
  }

  public async updateMovie(
    movieUuid: string,
    req: MovieRequest,
  ): Promise<Movie> {
    const movie = await this.movieRepository.findOne(movieUuid)

    if (!movie) {
      throw new NotFoundError('Movie not found')
    }

    movie.title = req.title
    movie.description = req.description
    movie.ratingClassificationId = req.ratingClassificationId
    movie.posterUrl = req.posterUrl
    movie.thumbnailUrl = req.thumbnailUrl
    movie.releaseDate = req.releaseDate

    return await this.movieRepository.save(movie)
  }

  public async addCrewToMovie(
    movieUuid: string,
    req: AddCrewRequest,
  ): Promise<void> {
    debug('Adding Crew to Movie', [movieUuid, req])
    const crew = this.crewRepository.create({
      movieUuid,
      personUuid: req.personUuid,
      crewRoleId: req.crewRoleId,
      name: req.name,
      notes: req.notes,
    })

    this.crewRepository.save(crew)
  }

  public async deleteCrewFromMovie(
    movieUuid: string,
    req: RemoveCrewRequest,
  ): Promise<void> {
    const result = await this.crewRepository.delete({
      movieUuid,
      personUuid: req.personUuid,
      crewRoleId: req.crewRoleId,
    })

    if (!result.affected) {
      throw new NotFoundError('Movie or person not found')
    }
  }

  public async addMovieRating(
    movieUuid: string,
    userUuid: string,
    req: AddRatingRequest,
  ): Promise<void> {
    const rating = this.ratingRepository.create({
      movieUuid,
      userUuid,
      rating: req.rating,
    })

    await this.ratingRepository.save(rating)
  }
}
