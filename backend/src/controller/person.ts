// import { plainToClass } from 'class-transformer'
import {
  JsonController,
  Get,
  ContentType,
  Post,
  Authorized,
  Body,
  Param,
  Delete,
} from 'routing-controllers'
import { OpenAPI } from 'routing-controllers-openapi'
import { ROLE_ADMIN } from 'src/constant'
import { Person } from 'src/models/Person'
import PersonService from 'src/services/person'
import { PersonRequest } from 'src/validators/person'
import { Service } from 'typedi'

@Service()
@JsonController('/api/person')
export default class PersonController {
  constructor(private personService: PersonService) {}

  @Get('s')
  @ContentType('application/json')
  public async getPersons(): Promise<Person[]> {
    return await this.personService.getPersons()
  }

  @Get('/:personUuid')
  @ContentType('application/json')
  public async getPerson(
    @Param('personUuid') personUuid: string,
  ): Promise<Person> {
    return await this.personService.getPerson(personUuid)
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Adds a new person',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Post('/')
  @ContentType('application/json')
  public async addPerson(@Body() req: PersonRequest): Promise<Person> {
    return this.personService.addPerson(req)
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Updates an existing person',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
      '404': {
        description: 'Person not found',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Post('/:personUuid')
  @ContentType('application/json')
  public async updatePerson(
    @Param('personUuid') personUuid: string,
    @Body() req: PersonRequest,
  ): Promise<Person> {
    return this.personService.updatePerson(personUuid, req)
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Deletes an existing person',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
      '404': {
        description: 'Person not found',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Delete('/:personUuid')
  public async deletePerson(
    @Param('personUuid') personUuid: string,
  ): Promise<string> {
    await this.personService.deletePerson(personUuid)
    return ''
  }
}
