import { plainToClass } from 'class-transformer'
import { JsonController, Get, ContentType } from 'routing-controllers'
import Database from 'src/database'
import { Service } from 'typedi'

class StatusResponse {
  status: boolean
  database: boolean
}

@Service()
@JsonController('/api/status')
export default class StatusController {
  constructor(private db: Database) {}
  /**
   * @returns {{ status: boolean }}
   * @memberof StatusRoute
   */
  @Get('/')
  @ContentType('application/json')
  public async getIndex(): Promise<StatusResponse> {
    let dbStatus = false
    try {
      await this.db.ping()
      dbStatus = true
    } catch (err) {
      console.log(err)
    }
    return plainToClass(StatusResponse, {
      status: dbStatus,
      database: dbStatus,
    })
  }
}
