import {
  JsonController,
  Get,
  ContentType,
  Param,
  Delete,
  Authorized,
  Body,
  Post,
  CurrentUser,
} from 'routing-controllers'
import UserService from 'src/services/user'
import { plainToClass } from 'class-transformer'
import { GetUserRequestModel, UserRoleRequest } from 'src/validators/user'
import { ROLE_ADMIN } from 'src/constant'
import { RequestResult } from 'src/interface/request-result'
import { User } from 'src/models/User'
import { Service } from 'typedi'
import { OpenAPI } from 'routing-controllers-openapi'

@Service()
@JsonController('/api/user')
export default class UserController {
  constructor(private userService: UserService) {}

  /**
   * @returns {Promise<UserInterface[]>}
   * @memberof UserController
   */
  @Authorized()
  @OpenAPI({
    description: 'List the available users',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Get('s')
  @ContentType('application/json')
  public async getUsers(): Promise<User[]> {
    return await this.userService.getUsers()
  }

  /**
   * @returns {Promise<unknown>}
   * @memberof UserController
   */
  @Authorized()
  @OpenAPI({
    description: 'Get the current user',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Get('/')
  @ContentType('application/json')
  public async getCurrentUser(@CurrentUser() user: User): Promise<User> {
    return user
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Retrieves a particular user',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Get('/:userUuid')
  @ContentType('application/json')
  public async getUser(@Body() req: GetUserRequestModel): Promise<User> {
    return await this.userService.getUser(req.userUuid)
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Delete a particular user',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Delete('/:userUuid')
  @ContentType('application/json')
  public async deleteUser(
    @Param('userUuid') userUuid: string,
  ): Promise<RequestResult> {
    return await this.userService.deleteUser(userUuid)
  }

  @Authorized()
  @OpenAPI({
    description: 'Delete the current user',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Delete('/')
  @ContentType('application/json')
  public async deleteCurrentUser(
    @CurrentUser() user: User,
  ): Promise<RequestResult> {
    return await this.userService.deleteUser(user.uuid)
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Add a role to a specific user',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Post('/:userUuid/role')
  public async addUserRole(
    @Param('userUuid') userUuid: string,
    @Body() body: { roleName: string },
  ): Promise<RequestResult> {
    const { roleName } = body
    return await this.userService.addRole(
      plainToClass(UserRoleRequest, {
        roleName,
        userUuid,
      }),
    )
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Remove a role to a specific user',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Delete('/:userUuid/role')
  public async delUserRole(
    @Param('userUuid') userUuid: string,
    @Body() body: { roleName: string },
  ): Promise<RequestResult> {
    const { roleName } = body
    return await this.userService.deleteRole(
      plainToClass(UserRoleRequest, {
        roleName,
        userUuid,
      }),
    )
  }
}
