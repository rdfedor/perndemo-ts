import {
  JsonController,
  Get,
  ContentType,
  Post,
  Authorized,
  Body,
  Param,
  Delete,
  CurrentUser,
} from 'routing-controllers'
import { OpenAPI } from 'routing-controllers-openapi'
import { ROLE_ADMIN } from 'src/constant'
import { Movie } from 'src/models/Movie'
import { User } from 'src/models/User'
import MovieService from 'src/services/movie'
import {
  AddCrewRequest,
  AddRatingRequest,
  MovieRequest,
  RemoveCrewRequest,
} from 'src/validators/movie'
import { Service } from 'typedi'

@Service()
@JsonController('/api/movie')
export default class MovieController {
  constructor(private movieService: MovieService) {}

  @Get('s')
  @ContentType('application/json')
  public async getMovies(): Promise<Movie[]> {
    return await this.movieService.getMovies()
  }

  @Get('/:movieUuid')
  @ContentType('application/json')
  public async getMovie(@Param('movieUuid') movieUuid: string): Promise<Movie> {
    return await this.movieService.getMovie(movieUuid)
  }

  @Authorized()
  @OpenAPI({
    description: 'Adds a new rating to a movie',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Post('/:movieUuid/rating')
  @ContentType('application/text')
  public async postMovieRating(
    @Param('movieUuid') movieUuid: string,
    @CurrentUser() user: User,
    @Body() req: AddRatingRequest,
  ): Promise<string> {
    await this.movieService.addMovieRating(movieUuid, user.uuid, req)
    return ''
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Adds a new movie',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Post('/')
  @ContentType('application/json')
  public async addMovie(@Body() req: MovieRequest): Promise<Movie> {
    return this.movieService.addMovie(req)
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Updates an existing movie',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
      '404': {
        description: 'Movie not found',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Post('/:movieUuid')
  @ContentType('application/json')
  public async updateMovie(
    @Param('movieUuid') movieUuid: string,
    @Body() req: MovieRequest,
  ): Promise<Movie> {
    return this.movieService.updateMovie(movieUuid, req)
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Deletes an existing movie',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
      '404': {
        description: 'Movie not found',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Delete('/:movieUuid')
  public async deleteMovie(
    @Param('movieUuid') movieUuid: string,
  ): Promise<string> {
    await this.movieService.deleteMovie(movieUuid)
    return ''
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Deletes an existing movie',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
      '404': {
        description: 'Movie or person not found',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Post('/:movieUuid/crew')
  public async addCrewToMovie(
    @Param('movieUuid') movieUuid: string,
    @Body() req: AddCrewRequest,
  ): Promise<string> {
    await this.movieService.addCrewToMovie(movieUuid, req)
    return ''
  }

  @Authorized([ROLE_ADMIN])
  @OpenAPI({
    description: 'Deletes an existing movie crew role',
    responses: {
      '403': {
        description: 'Unauthorized',
      },
      '404': {
        description: 'Movie or person not found',
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  })
  @Delete('/:movieUuid/crew')
  public async removeCrewFromMovie(
    @Param('movieUuid') movieUuid: string,
    @Body() req: RemoveCrewRequest,
  ): Promise<string> {
    await this.movieService.deleteCrewFromMovie(movieUuid, req)
    return ''
  }
}
