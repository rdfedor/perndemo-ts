import {
  JsonController,
  Post,
  ContentType,
  Body,
  Put,
  ForbiddenError,
} from 'routing-controllers'
import { CreateUserRequestModel } from 'src/validators/user'
import { AuthenticateUserRequest } from 'src/validators/auth'
import {
  SendPasswordResetRequest,
  ResetPasswordRequest,
} from 'src/validators/password-reset'
import {
  SendEmailVerificationRequest,
  EmailVerificationRequest,
} from 'src/validators/email-verification'
import { LoginToken } from 'src/validators/login-token'
import AuthService from '../services/auth'
import { RequestResult } from 'src/interface/request-result'
import { RefreshTokenRequest } from 'src/validators/auth'
import { ValidationError } from 'class-validator'
import { NotFoundError, AccessDeniedError } from 'src/error'
import UserService from 'src/services/user'
import { Service } from 'typedi'
import { ResponseSchema } from 'routing-controllers-openapi'

@Service()
@JsonController('/api/auth')
export default class AuthController {
  constructor(
    private authService: AuthService,
    private userService: UserService,
  ) {}

  @Post('/')
  @ContentType('application/json')
  @ResponseSchema(LoginToken)
  public async authenticateUser(
    @Body() req: AuthenticateUserRequest,
  ): Promise<LoginToken> {
    try {
      return await this.authService.processAuthenticateUserRequest(req)
    } catch (err) {
      if (
        err instanceof AccessDeniedError ||
        err instanceof NotFoundError ||
        (err instanceof Array &&
          err.filter(er => er instanceof ValidationError).length > 0)
      ) {
        throw new ForbiddenError('Invalid email and/or password.')
      }
      throw err
    }
  }

  @Post('/register')
  @ContentType('application/json')
  public async registerUser(
    @Body()
    user: CreateUserRequestModel,
  ): Promise<LoginToken> {
    return await this.authService.processRegisterUser(user)
  }

  @Post('/refresh')
  public async refreshToken(
    @Body() req: RefreshTokenRequest,
  ): Promise<LoginToken> {
    return await this.authService.processRefreshToken(req)
  }

  @Post('/reset-password')
  @ContentType('application/text')
  public async sendPasswordResetRequest(
    @Body() req: SendPasswordResetRequest,
  ): Promise<string> {
    return await this.authService.processSendPasswordResetRequest(req)
  }

  @Put('/reset-password')
  @ContentType('application/json')
  public async verifyResetPassword(
    @Body() req: ResetPasswordRequest,
  ): Promise<RequestResult> {
    return await this.authService.processResetPasswordRequest(req)
  }

  @Post('/verify-email')
  @ContentType('application/text')
  public async sendVerificationEmail(
    @Body() req: SendEmailVerificationRequest,
  ): Promise<string> {
    return await this.userService.processSendEmailVerificationRequest(req)
  }

  @Put('/verify-email')
  @ContentType('application/json')
  public async verifyForgotPassword(
    @Body() req: EmailVerificationRequest,
  ): Promise<RequestResult> {
    return this.userService.processEmailVerificationRequest(req)
  }
}
