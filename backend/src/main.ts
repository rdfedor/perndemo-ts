import 'reflect-metadata' // this shim is required

import * as http from 'http'

import ExpressLoader from './utils/expressLoader'
import {
  CompressPlugin,
  HelmetPlugin,
  NotFoundHandlerPlugin,
  RoutingControllersPlugin,
  SwaggerPlugin,
} from './plugins'
import { PORT } from './config'
import Container from 'typedi'
import Database from './database'

const database = Container.get(Database)

console.log('Establishing database connection...')
database.connect().then(() => {
  console.log('Starting service...')

  const loader = new ExpressLoader({
    plugins: [
      new HelmetPlugin(),
      new RoutingControllersPlugin(),
      new SwaggerPlugin(),
      new NotFoundHandlerPlugin(),
      new CompressPlugin(),
    ],
  })

  const server = http.createServer(loader.getApp())

  loader.loadPlugins()

  server.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
  })
})
