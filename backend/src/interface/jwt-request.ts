import { RequestHandler } from 'express'

export interface JwtToken {
  userUuid: string
  email: string
  roles: [string]
}

export interface JwtRequestInterface extends RequestHandler {
  token: JwtToken
}
