import { Express } from 'express'

export default interface ExpressPlugin {
  use(app: Express): void
}
