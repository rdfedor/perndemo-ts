import { createConnection, Connection, useContainer } from 'typeorm'
import { Container } from 'typeorm-typedi-extensions'
import { Service } from 'typedi'

@Service()
export default class Database {
  private connection: Connection

  constructor() {
    useContainer(Container)
  }

  public async connect(): Promise<Connection> {
    this.connection = await createConnection()

    return this.connection
  }

  public async ping(): Promise<unknown> {
    return await this.connection.query('SELECT 1')
  }
}
