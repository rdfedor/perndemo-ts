import { IsNotEmpty, IsEmail, MaxLength, Length, IsJWT } from 'class-validator'

export class AuthenticateUserRequest {
  @IsNotEmpty()
  @IsEmail()
  @MaxLength(254)
  public email: string

  @IsNotEmpty()
  @Length(8)
  public password: string
}

export class RefreshTokenRequest {
  @IsNotEmpty()
  @IsJWT()
  public token: string
}
