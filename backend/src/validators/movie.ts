import {
  IsDateString,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUrl,
  IsUUID,
} from 'class-validator'

export class MovieRequest {
  @IsNotEmpty()
  title: string

  @IsOptional()
  description: string

  @IsNotEmpty()
  @IsNumber()
  ratingClassificationId: number

  @IsUrl()
  @IsOptional()
  thumbnailUrl: string

  @IsUrl()
  @IsOptional()
  posterUrl: string

  @IsDateString()
  @IsOptional()
  releaseDate: Date
}

export class AddCrewRequest {
  @IsNotEmpty()
  @IsUUID()
  personUuid: string

  @IsNotEmpty()
  @IsNumber()
  crewRoleId: number

  @IsString()
  @IsOptional()
  name: string

  @IsString()
  @IsOptional()
  notes: string
}

export class RemoveCrewRequest {
  @IsNotEmpty()
  @IsUUID()
  personUuid: string

  @IsNotEmpty()
  @IsNumber()
  crewRoleId: number
}

export class AddRatingRequest {
  @IsNotEmpty()
  @IsNumber()
  rating: number
}
