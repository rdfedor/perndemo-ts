import {
  IsUUID,
  IsNotEmpty,
  IsEmail,
  MinLength,
  MaxLength,
  IsJWT,
  Length,
} from 'class-validator'

export class GetUserRequestModel {
  @IsNotEmpty()
  @IsUUID('all')
  userUuid: string
}

export class GetUserByEmailRequestModel {
  @IsNotEmpty()
  @IsEmail()
  email: string
}

export class DeleteUserRequestModel {
  @IsNotEmpty()
  @IsUUID('all')
  userUuid: string
}

export class CreateUserRequestModel {
  @IsNotEmpty()
  @MaxLength(100)
  firstName: string
  @IsNotEmpty()
  @MaxLength(100)
  lastName: string
  @IsJWT()
  @IsNotEmpty()
  token: string
  @IsNotEmpty()
  @MinLength(8)
  password: string
  @IsNotEmpty()
  @Length(6)
  challengeCode: string
}

export class UserRoleRequest {
  @IsNotEmpty()
  @IsUUID('all')
  public userUuid: string

  @IsNotEmpty()
  public roleName: string
}
