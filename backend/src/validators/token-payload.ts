export default class TokenPayload {
  userUuid: string
  email: string
  roles: string[]
}
