import {
  IsDateString,
  IsLatLong,
  IsNotEmpty,
  IsOptional,
} from 'class-validator'

export class PersonRequest {
  @IsNotEmpty()
  name: string

  @IsNotEmpty()
  @IsOptional()
  description: string

  @IsOptional()
  @IsLatLong()
  location: string

  @IsDateString()
  @IsOptional()
  born: string

  @IsDateString()
  @IsOptional()
  died: string
}
