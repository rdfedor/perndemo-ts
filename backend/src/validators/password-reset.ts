import {
  IsNotEmpty,
  IsEmail,
  MaxLength,
  IsJWT,
  MinLength,
} from 'class-validator'

export class SendPasswordResetRequest {
  @IsNotEmpty()
  @IsEmail()
  @MaxLength(254)
  public email: string
}

export class ResetPasswordRequest {
  @IsNotEmpty()
  @IsJWT()
  public token: string

  @IsNotEmpty()
  @MinLength(8)
  public password: string
}
