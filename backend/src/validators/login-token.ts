export class LoginToken {
  accessToken: string
  refreshToken: string
  maxAge?: string | number
  iat?: number
  roles?: string[]
  userUuid: string
  email: string
  firstName?: string
  lastName?: string
  verifiedEmail?: boolean
}
