import { EntityRepository, Repository } from 'typeorm'
import { Service } from 'typedi'
import { Role } from '../models/Role'

@Service()
@EntityRepository(Role)
export class RoleRepository extends Repository<Role> {}
