import { EntityRepository, Repository } from 'typeorm'
import { Service } from 'typedi'
import { Crew } from '../models/Crew'

@Service()
@EntityRepository(Crew)
export class CrewRepository extends Repository<Crew> {}
