import { EntityRepository, Repository } from 'typeorm'
import { Service } from 'typedi'
import { Movie } from '../models/Movie'

@Service()
@EntityRepository(Movie)
export class MovieRepository extends Repository<Movie> {}
