import { EntityRepository, Repository } from 'typeorm'
import { Service } from 'typedi'
import { User } from '../models/User'

@Service()
@EntityRepository(User)
export class UserRepository extends Repository<User> {
  public addRole(
    userUuid: string,
    roleName: string,
    targetUuid: string = null,
    targetId: number = null,
  ): Promise<unknown> {
    return this.query(
      `
      INSERT INTO user_role (user_uuid, role_id, target_uuid, target_id)
      SELECT $1, role_id, $3, $4 FROM Role WHERE name = $2
    `,
      [userUuid, roleName, targetUuid, targetId],
    )
  }

  public deleteRole(
    userUuid: string,
    roleName: string,
    targetUuid: string = null,
    targetId: number = null,
  ): Promise<unknown> {
    const queryParams = [userUuid, roleName]
    let extraSql = ''

    if (targetUuid) {
      queryParams.push(targetUuid)
      extraSql = 'AND target_uuid = $3'
    } else if (targetId) {
      queryParams.push(targetId.toString())
      extraSql = 'AND target_id = $3'
    }

    return this.query(
      `
      DELETE FROM user_role WHERE user_uuid = $1 AND role_id = (
        SELECT role_id from role WHERE name = $2
      ) ${extraSql}
    `,
      queryParams,
    )
  }
}
