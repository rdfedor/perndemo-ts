import { EntityRepository, Repository } from 'typeorm'
import { Service } from 'typedi'
import { Rating } from 'src/models/Rating'

@Service()
@EntityRepository(Rating)
export class RatingRepository extends Repository<Rating> {}
