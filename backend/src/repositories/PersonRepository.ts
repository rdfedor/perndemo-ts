import { EntityRepository, Repository } from 'typeorm'
import { Service } from 'typedi'
import { Person } from 'src/models/Person'

@Service()
@EntityRepository(Person)
export class PersonRepository extends Repository<Person> {}
