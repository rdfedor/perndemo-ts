import {
  Middleware,
  ExpressErrorMiddlewareInterface,
  // ExpressMiddlewareInterface,
} from 'routing-controllers'
import { Request, Response /*, RequestHandler */ } from 'express'
import { ENV } from 'src/config'
import { ENVIRONMENT_PROD } from 'src/constant'
import { ValidationError } from 'class-validator'
import { Service } from 'typedi'

@Service()
@Middleware({ type: 'after' })
export class CustomErrorHandler implements ExpressErrorMiddlewareInterface {
  error(err: Error, _req: Request, res: Response): void {
    const json: {
      message: string
      status: number
      details?: string
      errors?: [unknown]
    } = {
      status: 500,
      message: '',
    }
    if (
      err instanceof Array &&
      !err.filter(inst => !(inst instanceof ValidationError)).length
    ) {
      json.status = 400
      json.message = err
        .map(inst =>
          Object.keys(inst.constraints)
            .map(key => inst.constraints[key])
            .join(', '),
        )
        .join('. ')
      if (err['errors']) {
        json.errors = err['errors']
      }
    } else {
      json.status = err['httpCode'] || json.status
      json.message = err['message'].toString()

      if (err['errors']) {
        json.errors = err['errors'].map((err: ValidationError) => ({
          property: err.property,
          constraints: err.constraints,
        }))
      }

      if (ENV !== ENVIRONMENT_PROD && json.status === 500) {
        json.details = err.stack
      }
    }

    res.status(json.status)
    res.json(json)

    console.log(JSON.stringify(err))
  }
}
