module.exports = [
  {
    name: 'default',
    type: process.env.DB_TYPE || 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || 5432,
    username: process.env.DB_USER || 'pg',
    password: process.env.DB_PASS || 'test',
    database: process.env.DB_NAME || 'test',
    logging: process.env.DB_LOGGING || false,
    entities: ['src/models/*.ts'],
    migrations: ['migrations/run-once/*.ts'],
    migrationsRun: false,
    cli: {
      migrationsDir: 'migrations/run-once',
      entitiesDir: 'src/models',
    },
  },
]
