import { readFile } from 'fs'
import { glob } from 'glob'
import { createConnection } from 'typeorm'
import { promisify } from 'util'

const pGlob = promisify(glob)
const pReadFile = promisify(readFile)

const command = async () => {
  const connection = await createConnection()
  return connection.transaction(async transaction => {
    const files = await pGlob('./migrations/run-always/*.sql')
    return Promise.all(
      files.map(async file => {
        const sql = await pReadFile(file)
        console.log(`Running ${file}`)
        return transaction.query(sql.toString())
      }),
    )
  })
}

console.log('Loading repeatable migrations')
command()
  .then(() => {
    console.log('Done running repeatable migrations')
    process.exit()
  })
  .catch(err => {
    console.log(err.message)
    process.exit(1)
  })
