# Docker Typescript Boilerplate

## Table of Contents

1. [Features](#features)
2. [Folder Locations](#folder-locations)
3. [Migrations](#migrations)
    1. [Run-Once Migrations](#Run-Once-Migrations)
    2. [Run-Always Migrations](#Run-Always-Migrations)
    3. [Rollback Strategy](#rollback-strategy)
    4. [Using the CLI](#using-the-cli)

## Features

* Model-Controller-Service folder structure with decorator based routing
* Allows for creations and validation of JSON Web Key Sets
* Role based user authentication and controller routing
* Global error handling middleware
* Dependency injection
* Runtime built documentation using class reflections to build Swagger documentation
* Password hashing through bcrypt

## Folder Locations

* __ tests __ - Folder containing the jest unit tests for the project.
* migrations/run-once - Run once and run always migrations
* migrations/run-always - Run-always scripts which is executed on start-up
* src/controller - Definition of controllers with route based decorators
* src/models - defines typeorm models  for a database
* src/plugins - defines plugins for the express loader
* src/repository - defines repositories that can be used to interact with the models and add custom database handlers 
* src/services - defines services which the controllers will call which performs operations against repositories
* src/validators - Request validators used to validate incoming requests from the user

## Migrations

### Run-Once Migrations

Migrations related to adding and removing tables, columns and indexes. Contains both an up function for moving a migration forward and a down for rolling back the changes.  Migrations should always be additive to way a database functions but with a rollback strategy that reverts the database changes back to their pre-migration state.  If a table is being re-written entirely, the old table should be renamed as opposed to being dropped entirely that way if a rollback is necessary, data that was added to the new table can be transfered to the old, drop the table created by the migration and renaming the old back to its original state. On the next release, once it's decided that everything was ran properly and the old table is no longer needed, another run-once migration can be written to drop it.

### Run-Always Migrations

Migrations related to creating and updating views, functions and stored procedures. Run-Always migration changes are tracked through source control and tagged via docker images.

### Rollback Strategy

In the event a rollback is necessary in a live environment, it can be accomplished by using the backend-migrations container to trigger a `yarn migrate:down`.  Once all the changes are reverted back to its pre-deployment state, deploy the previous image and re-run the migration to rollback the views, functions and stored procedures in the run-always migration to their previous state.

### Using the CLI

TypeORM comes with a cli tool to allow for the creation of new run-once migrations.  To create a new migration, run the following command which will create the migration in the run-once directory and add a unix timestamp to the beginning of the filename,

```sh
npx typeorm migration:create --name MyNewMigration
```
